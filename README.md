## Read light-dependent resistor

Simple application to read the A/D converter attached to the
junction of an LDR and resistor and display the result.

The shield contains the LDR, a DS1801 temperature sensor and
a 24FC128 serial EEPROM.

---

![shield](images/Shield.png)

**Shield schematic showing temperature sensor and serial EEPROM as well as LDR**

---

Three results are displayed as a percentage of full scale:
* Raw ADC result
* Smoothed result, alpha = 0.1
* Average of 10 samples

In addition, a bar graph of the raw data is displayed

---

![Display](images/LDR-display.jpg)

**Result Display**

---

There are three branches in this repository, each representing an 
alternate way to read a single input as described in the Family 
Reference Manual:

* master - Sampling is started and stopped manually, conversion occurs automatically at the completion of sampling

* manual - Sampling and conversion each started manually

* auto - Sampling and conversion started automatically, A/D converter free runs


