/*! \file  initializeClock.c
 *
 *  \brief Set the clock to 70 MIPS
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 9:53 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! initializeClock - */

/*!
 *
 */
void initializeClock(void)
{
  /* Set the clock to 70 MIPS so we know timing                 */
  CLKDIVbits.FRCDIV = 0;        /* Divide by 1 = 8MHz           */
  CLKDIVbits.PLLPRE = 0;        /* Divide by 2 = 4 MHz          */
  PLLFBD = 74;                  /* Multiply by 72 = 140         */
  CLKDIVbits.PLLPOST = 0;       /* Divide by 2 = 70             */

}
