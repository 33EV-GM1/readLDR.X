/*! \file  initializeADC.c
 *
 *  \brief Initialize the A/D converter
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 10:00 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! initializeADC - */

/*!
 *
 */
void initializeADC(void)
{
  // ADC Control registers)
  AD1CON1bits.ASAM = 1;     // Use automatic sampling
  AD1CON3bits.ADRC = 1;     // Use A/D RC clock
  AD1CON3bits.SAMC = 31;    // Auto sample 31Tad
  // A/D Sample Select Register
  AD1CHS0bits.CH0NB = 0; /* Channel 0 negative input is Vrefl */
  AD1CHS0bits.CH0SB = 0; /* Channel 0 positive input is AN0 */

  AD1CON1bits.ADON = 1;     // Turn on ADC
}
