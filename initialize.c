/*! \file  initialize.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 11:17 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"
#include "readLDR.h"

/*! initialize - */

/*!
 *
 */
void initialize(void)
{
  initializeClock();
    
  /* Show some start up stuff on the LCD just because */
  LCDinit();
  LCDclear();
  LCDputs("Read LDR");
  Delay_ms(1000);
  
  initializeADC();

}
