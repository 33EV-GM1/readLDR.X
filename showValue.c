/*! \file  showValue.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 10:56 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include "../include/LCD.h"

/*! showValue - */

/*!
 *
 */
void showValue(double fValue, int nPosition)
{
  char szWork[32];

  sprintf(szWork, "%4.1f", fValue);
  LCDposition(nPosition);
  LCDputs(szWork);
}
