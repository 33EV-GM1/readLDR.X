/*! \file  readLDR.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 9:54 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef READLDR_H
#define	READLDR_H

#ifdef	__cplusplus
extern "C"
{
#endif

void initialize(void);
void initializeClock(void);
void initializeADC(void);
void showValue(double, int);
void showBarGraph(double);

#define ALPHA 0.1
#define NUMSAMPLES 10


#ifdef	__cplusplus
}
#endif

#endif	/* READLDR_H */

