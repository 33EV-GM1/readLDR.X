/*! \file  showBarGraph.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date December 10, 2015, 11:07 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "../include/LCD.h"

/*! showBarGraph - */

/*!
 *
 */
void showBarGraph(double fPercent)
{
  char szWork[32];
  int i;

  szWork[0] = '\0';
  for (i = 0; i < (int) (0.16 * fPercent + 0.5); i++)
    strcat(szWork, "\377");
  strcat(szWork, "               ");
  szWork[16] = '\0';
  LCDhome();
  LCDputs(szWork);
}
