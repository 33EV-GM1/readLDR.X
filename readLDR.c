/*! \file  readLDR.c
 *
 *  \brief  Read the light dependent resistor (LDR)
 * 
 * Assumes the LDR on A0. The high end of the LDR is connected to
 * Vdd and the low end through a 2.37K to Vss.
 * 
 * In this version, sampling is started manually but conversion occurs
 * automatically when sampling is complete.
 *
 *  \author jjmcd
 *  \date September 18, 2015, 5:15 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <string.h>
#include "../include/LCD.h"
#include "configBits.h"
#include "readLDR.h"

/*! main - Read light dependent resistor */

/*! Reads AN0 which is connected to an LDR.  Displays the percentage reading
 *  raw, smoothed and averaged.
 * 
 *  With room light on the vertical board reads around 680.  Orienting
 *  the board to point to the light moves it up to around 750.  Shining
 *  an LED flashlight directly on the resistor give a reading of around
 *  940.  Placing a finger over the sensor causes the reading to drop
 *  to around 100.  Given that the range of the 10-bit A/D is 1024,
 *  the 2K31 voltage divider looks to be a reasonable choice.
 */
int main(void)
{
  double fSmoothed;
  int nSampleCount;
  int nAverage;
  double fPercent;
  
  initialize();
  
  /* Clear out result */
  fSmoothed = 0;
  nAverage = 0;
  nSampleCount = 0;
  
  while(1)
    {
      /* Delay mostly for the display, but we need at least 100uS for sampling */
      Delay_ms(20);
      /* End sampling and start conversion */
      AD1CON1bits.SAMP = 0;     /* Start conversion */
      /* Wait for conversion complete */
      while ( !AD1CON1bits.DONE )
        ;
      /* Display the A/D result on line 2 of the LCD */
      fPercent = (double)ADC1BUF0 / 10.24;
      showValue(fPercent,0x40);
      
      /* Calculate the smoothed value */
      fSmoothed = (1.0-ALPHA)*fSmoothed + ALPHA*(double)ADC1BUF0;
      
      /* Add into the averaged value */
      nAverage = nAverage + ADC1BUF0;
      nSampleCount++;
      
      /* Clear conversion done status bit which starts sampling */
      AD1CON1bits.DONE = 0;     /* Clear conversion done status bit */

      showBarGraph(fPercent);
      
      /* Display the smoothed result */
      fPercent = (double)fSmoothed / 10.24;
      showValue(fPercent,0x46);
      
      /* If we have NUMSAMPLES samples, display the average */
      if ( nSampleCount == NUMSAMPLES )
        {
          nAverage = nAverage / NUMSAMPLES;
          fPercent = (double)nAverage / 10.24;
          showValue(fPercent,0x4c);
          nSampleCount = 0;
          nAverage = 0;
        }
    }
  return 0;
}
